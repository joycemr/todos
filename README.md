### todos application for Launch Code class

## Directories

# api
- the Java/Spring endpoints

# app
- the Javascript user front end

## Top level files
- docker-compose
	- builds the entire application, including a containerized postgreSQL database
