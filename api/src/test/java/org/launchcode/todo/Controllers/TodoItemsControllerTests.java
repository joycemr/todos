package org.launchcode.todo.Controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.launchcode.todo.IntegrationTestConfig;
import org.launchcode.todo.Models.RawItem;
import org.launchcode.todo.Models.TodoItem;
import org.launchcode.todo.data.TodosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@IntegrationTestConfig
public class TodoItemsControllerTests {

	@Autowired
	private TodosRepository todosRepository;

	@Autowired
	private MockMvc mockRequest;

	TodoItem newItem;

	private final String getPath = TodoItemsController.ROOT_PATH;
	private final String postPath = TodoItemsController.ROOT_PATH + "/";
	private final String patchPath = TodoItemsController.ROOT_PATH + "/";
	private final String deletePath = TodoItemsController.ROOT_PATH + "/";

	@BeforeEach
	public void setupTest() {
		this.newItem = new TodoItem();
		this.newItem.setText("pick up milk");
		todosRepository.save(this.newItem);
	}

	@AfterEach
	public void teardownTest() {
		todosRepository.deleteAll();
	}

	@Test
	public void getTodos() throws Exception {
		mockRequest.perform(MockMvcRequestBuilders.get(getPath))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$[0].id").value(newItem.getId()))
			.andExpect(jsonPath("$[0].text").value(newItem.getText()))
			.andExpect(jsonPath("$[0].completed").value(false));
	}

	@Test
	@DisplayName("POST /todos (RawItem): creates and returns a JSON representation of the new todos entity")
	public void createTodoItem() throws Exception {
		long beforeRowCount = todosRepository.count();
		RawItem rawItem = new RawItem("Pick up milk");
		mockRequest.perform(MockMvcRequestBuilders.post(postPath)
				.contentType(MediaType.APPLICATION_JSON)
					.content(new ObjectMapper().writeValueAsString(rawItem)))
			.andExpect(status().isCreated())
			.andExpect(jsonPath("$.id").isNumber())
			.andExpect(jsonPath("$.text").value(rawItem.getText()))
			.andExpect(jsonPath("$.completed").value(false));
		assertEquals(beforeRowCount + 1, todosRepository.count());
	}

	@Test
	@DisplayName("Patch /todos (RawItem): update a todo item to completed")
	public void updateTodoItem() throws Exception {
		mockRequest.perform(MockMvcRequestBuilders.patch(patchPath + newItem.getId()))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.id").value(newItem.getId()))
			.andExpect(jsonPath("$.completed").value(true));
		assertTrue(todosRepository.getOne(newItem.getId()).getCompleted());
	}

	@Test
	@DisplayName("Patch /todos (RawItem): update a non-existent todo item to completed")
	public void updateTodoItemFail() throws Exception {
		mockRequest.perform(MockMvcRequestBuilders.patch(patchPath + "-1"))
			.andExpect(status().isNotFound());
	}

	@Test
	@DisplayName("Delete /todos (RawItem): Insert a new todo and then delete it")
	public void deleteTodoItem() throws Exception {
		mockRequest.perform(MockMvcRequestBuilders.delete(deletePath + newItem.getId()))
			.andExpect(status().isOk());
		assertFalse(todosRepository.existsById(newItem.getId()));
	}

	@Test
	@DisplayName("Patch /todos (RawItem): delete a non-existant todo item to completed")
	public void deleteTodoItemFail() throws Exception {
		mockRequest.perform(MockMvcRequestBuilders.patch(deletePath + "-1"))
			.andExpect(status().isNotFound());
	}

}
