package org.launchcode.todo.Controllers;

import java.util.List;
import java.util.Optional;

import org.launchcode.todo.Models.RawItem;
import org.launchcode.todo.Models.TodoItem;
import org.launchcode.todo.data.TodosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*", allowCredentials = "true", allowedHeaders = "*")
@RequestMapping(TodoItemsController.ROOT_PATH)
public class TodoItemsController {

  public static final String ROOT_PATH = "/todos";

  @Autowired
  private TodosRepository todosRepository;

  @GetMapping(produces = "application/json")
  public ResponseEntity<List<TodoItem>> getIndex() {
    return new ResponseEntity<>(todosRepository.findAll(), HttpStatus.OK);
  }

  @PostMapping(consumes = "application/json", produces = "application/json")
  public ResponseEntity<TodoItem> postTodoItem(@RequestBody RawItem rawItem) { // model binding
    TodoItem newTodo = new TodoItem();
    newTodo.setText(rawItem.getText());
    return new ResponseEntity<TodoItem>(todosRepository.save(newTodo), HttpStatus.CREATED);
  }

  @PatchMapping(path = "/{id}")
  public ResponseEntity<TodoItem> completeTotoItem(@PathVariable int id) {
    Optional<TodoItem> optionalTodoItem = todosRepository.findById(id);
    if (!optionalTodoItem.isPresent()) {
      return new ResponseEntity<TodoItem>(HttpStatus.NOT_FOUND);
    }
    TodoItem todoItem = optionalTodoItem.get();
    todoItem.setCompleted(true);
    todosRepository.save(todoItem);
    return new ResponseEntity<TodoItem>(todoItem, HttpStatus.OK);
  }

  @DeleteMapping(path = "/{id}")
  public ResponseEntity<String> deleteTotoItem(@PathVariable int id) {
    if (!todosRepository.existsById(id)) {
      return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
    }
    todosRepository.deleteById(id);
    return new ResponseEntity<String>(HttpStatus.OK);
  }

}
