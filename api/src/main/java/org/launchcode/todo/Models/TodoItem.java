package org.launchcode.todo.Models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class TodoItem {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "todos_seq")
  private int id;
  private String text;
  @Column(columnDefinition = "boolean default false")
  private boolean completed;

  public TodoItem() {}

  public int getId() {
    return this.id;
  }

  public String getText() {
    return this.text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public boolean getCompleted() {
    return this.completed;
  }

  public void setCompleted(boolean completed) {
    this.completed = completed;
  }

}
