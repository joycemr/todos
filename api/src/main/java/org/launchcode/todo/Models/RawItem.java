package org.launchcode.todo.Models;

public class RawItem {
  private String text;

  public RawItem() {
  }

  public RawItem(String text) {
    this.text = text;
  }

  public String getText() {
    return this.text;
  }

  public void setText(String text) {
    this.text = text;
  }

}
