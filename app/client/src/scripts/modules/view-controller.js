import { incompleteList, completedList, newTodoForm, todoText } from "./dom-constants.js"
import { getToDos, addToDo, updateToDoState, deleteToDo } from "./data-controller.js"
import { createIncompleteListItemComponent, createCompletedListItemComponent } from "./create-list-item-utils.js"


export const renderTodoList = () => {
	getToDos()
		.then(toDoData => {
			toDoData.forEach(element => {
				if (element.completed) {
					const li = createCompletedListItemComponent(element, handleDeletion);
					completedList.appendChild(li);
				} else {
					const li = createIncompleteListItemComponent(element, handleMarkingComplete);
					incompleteList.appendChild(li);
				}
			});
		})
		.catch(console.log);
}

export const addNewTodoItem = (event) => {
	event.preventDefault(); // do I need this?
	const payload = { text: todoText.value };
	addToDo(payload)
		.then((newTodoItem) => {
			const li = createIncompleteListItemComponent(newTodoItem, handleMarkingComplete
			);
			incompleteList.appendChild(li);
		})
		.catch(console.log);
	newTodoForm.reset();
};

const handleMarkingComplete = (event) => {
	updateToDoState(event.target.getAttribute("todoId"))
		.then((updatedTodoItem) => {
			if (!updatedTodoItem.completed) {
				console.log("updateToDoState Error: item " + updatedTodoItem.id + " was not updated");
			}
			removeToDo(updatedTodoItem.id);
			const li = createCompletedListItemComponent(updatedTodoItem, handleDeletion);
			completedList.appendChild(li);
		})
		.catch(console.log);
};

const handleDeletion = (event) => {
	const id = event.target.getAttribute("todoId");
	deleteToDo(id)
	removeToDo(id);
};

function removeToDo(id) {
	document.querySelector(`[todoId='${id}']`).parentElement.remove();
};
