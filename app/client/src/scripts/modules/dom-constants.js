export const incompleteList = document.querySelector("#incomplete-list");
export const completedList = document.querySelector("#completed-list");
export const todoText = document.querySelector("#todo-text");
export const newTodoForm = document.querySelector("#new-todo-form");