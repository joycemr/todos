const url = "http://localhost:8001";
const path = "/todos";
const endpoint = url + path;

/**
 * getToDos - return the data from the TODOS endpoint
 * @return Promise
 * @return Array - A list of todo objects
 */
export function getToDos() {
  return fetch(endpoint)
  .then(response => response.json())
  // .then(data => data) // do I need this line?
  .catch(error => `getToDos: Fetch error: ${error}`);
}

// Why async?

/**
 * addToDo - add a new item to the TODOS endpoint
 * @return The toDo item as an object
 */
// Why am I not returning the status?
// Why not just return the promise?
export function addToDo(data = {}) {
  return fetch(endpoint, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data)
  })
    .then(response => response.json())
    .then(data => data)
    .catch(error => `postToDos: Fetch error: ${error}`);
};

/**
 * updateToDoState - update an item to completed: true
 * @return The toDo item as an object
 */
export function updateToDoState(id) {
  return fetch(endpoint + "/" + id, {
    method: 'PATCH'
  })
  .then(response => response.json())
  .then(data => data)
  .catch(error => `patchToDos: Fetch error: ${error}`);
}

/**
 * deleteToDo - delete a completed item
 * @return boolean
 */
export function deleteToDo(id) {
  fetch(endpoint + "/" + id, {
    method: 'DELETE'
  })
    .then(response => {
      if (response.status === 404) {
        console.log("todo item " + id + " not found");
        return false;
      }
    })
    .catch(error => `patchToDos: Fetch error: ${error}`);
  return true;
};
