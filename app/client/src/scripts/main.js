import {renderTodoList, addNewTodoItem} from "./modules/view-controller.js"
import {newTodoForm} from "./modules/dom-constants.js"

document.addEventListener("DOMContentLoaded", (event) => {

  renderTodoList();

  newTodoForm.addEventListener("submit", (event) => addNewTodoItem(event));

});
